export default {
  props: ['item', 'tags'],
  emits: ['delete-item'],
  data() {
    return {
      unfold: false,
      newattrname: "",
      err: false,
      stylelist: ["bold", "italic", "underline"],
      colorlist: ["black", "silver", "gray", "white", "maroon", "red", "purple", "fuchsia", "green", "lime", "olive", "yellow", "navy", "blue", "teal", "aqua"],
    }
  },
  methods: {
    reformatTag(tagname){
      return tagname.replace(/:/i, '-')
    },
    toggleFold() {
      this.unfold = !this.unfold
    },
    addchild(child) {
      if (!(this.item.children.includes(child))) {
        this.item.children.push(child)
      }
    },
    deletechild(child) {
      if (this.item.children.includes(child)) {
        this.item.children.splice(this.item.children.indexOf(child), 1)
      }
    },
    addattr() {
      if (!this.item.attributes.some((element) => element.key == this.newattrname)) {
        this.item.attributes.push({
          'key': this.newattrname,
          'type': "enumerated",
          'required': false,
          'values': []
        })
        this.newattrname = ""
      } else {
        this.err = true
      }

    },
    deleteattr(attribute) {
      this.item.attributes.splice(this.item.attributes.indexOf(attribute), 1)
    },
    removeError() {
      this.err = false
    },
    changeRoot() {
      this.item.rootDisplayed = !this.item.rootDisplayed
    },
    switchstyleitem(item) {
      if (this.item.style.includes(item)) {
        this.item.style.splice(this.item.style.indexOf(item), 1)
      } else {
        this.item.style.push(item)
      }
    },
    addAllChildren() {
      for (const element of this.tags) {
        if (!this.item.children.includes(element.tag)) {
          this.item.children.push(element.tag)
        }
      }
    },
    deleteAllChildren() {
      this.item.children = []
    }
  },
  template: `
  <div class="col mb-2">
    <div class="card itemlist">
      <div class="card-body">
        <h5 class="card-title text-center">
          <span class="text-muted">&lt;</span>
          {{item.tag}}
          <span class="text-muted">&gt;</span>
          <button class="btn btn-sm btn-secondary float-end" @click="toggleFold" v-if="!unfold">▼</button>
          <button class="btn btn-sm btn-secondary float-end" @click="toggleFold" v-if="unfold">▲</button>
        </h5>

        <template v-if="unfold">
          <!--
          <div class="input-group mt-4">
            <span class="input-group-text">Nom</span>
            <input class="form-control" v-model="item.tag" placeholder="Nom de l'élément"/>
          </div>
          -->

          <div class="accordion mt-2" v-bind:id="'accordion'+reformatTag(item.tag)">

            <div class="accordion-item">
              <h2 class="accordion-header">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" :data-bs-target='"#collapse-help"+reformatTag(item.tag)'>
                  Message d'aide
                </button>
              </h2>
              <div :id="'collapse-help'+ reformatTag(item.tag)" class="accordion-collapse collapse" :data-bs-parent="'#accordion'+reformatTag(item.tag)">
                <div class="accordion-body">
                  <textarea class="form-control" v-model="item.help" placeholder="texte d'aide (optionnel)"/>
                </div>
              </div>
            </div>

            <div class="accordion-item">
              <h2 class="accordion-header" id="headingTwo">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" :data-bs-target="'#collapseTwo'+reformatTag(item.tag)">
                  Attributs
                </button>
              </h2>
              <div :id="'collapseTwo'+reformatTag(item.tag)" class="accordion-collapse collapse" :data-bs-parent="'#accordion'+reformatTag(item.tag)">
                <div class="accordion-body">

                <div><b>Ajout d'un nouvel attribut:</b></div>
                <div class="input-group">
                  <input class="form-control" placeholder="name of new attribute" v-model="newattrname"/>
                  <button v-if="newattrname!=''" class="btn btn-outline-secondary" type="button" id="button-addon1" @click="addattr">+</button>
                </div>
                <div v-if="err">Erreur: l'attribut indiqué existe déjà!</div>
                <button @click="removeError" v-if="err">OK</button>

                <ul class="mt-4 list-group">
                  <tag-attribute v-for="attribute in item.attributes" v-bind:attribute="attribute" @deleteattr="deleteattr"></tag-attribute>
                </ul>

                </div>
              </div>
            </div>
            <div class="accordion-item">
              <h2 class="accordion-header" id="headingThree">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" :data-bs-target="'#collapseThree'+reformatTag(item.tag)">
                  Sous-éléments
                </button>
              </h2>
              <div :id="'collapseThree'+reformatTag(item.tag)" class="accordion-collapse collapse" :data-bs-parent="'#accordion'+reformatTag(item.tag)">
                <div class="accordion-body">
                  <tag-children v-for="tag in tags" v-bind:child="tag.tag" v-bind:parent="item.tag" v-bind:childlist="item.children" @newchild="addchild" @removechild="deletechild"></tag-children>

                  <div class="text-center mt-2">
                    <div class="btn-group" role="group">
                        <button class="btn btn-sm btn-outline-secondary" @click="addAllChildren()">Tout ajouter</button>
                        <button class="btn btn-sm btn-outline-secondary" @click="deleteAllChildren()">Tout retirer</button>
                    </div>
                  </div>

                </div>
              </div>
            </div>

            <div class="accordion-item">
              <h2 class="accordion-header" id="heading-4">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" :data-bs-target="'#collapseFour'+reformatTag(item.tag)">
                  Autres options
                </button>
              </h2>
              <div :id="'collapseFour'+reformatTag(item.tag)" class="accordion-collapse collapse" :data-bs-parent="'#accordion'+reformatTag(item.tag)">
                <div class="accordion-body">
                  <div v-if="!item.rootDisplayed">Cet étiquette ne peut être utilisée à la racine</div>
                  <div v-if="item.rootDisplayed">Cet étiquette peut être utilisée à la racine</div>
                  <button class="btn btn-sm btn-outline-secondary" @click="changeRoot" v-if="!item.rootDisplayed">Autoriser utilisation à la racine</button>
                  <button class="btn btn-sm btn-outline-secondary" @click="changeRoot" v-if="item.rootDisplayed">Interdire utilisation à la racine</button>
                </div>
              </div>
            </div>

            <div class="accordion-item">
              <h2 class="accordion-header" id="heading-style">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" :data-bs-target="'#collapseStyle'+reformatTag(item.tag)">
                  Styles
                </button>
              </h2>
              <div :id="'collapseStyle'+reformatTag(item.tag)" class="accordion-collapse collapse" :data-bs-parent="'#accordion'+reformatTag(item.tag)">
                <div class="accordion-body">
                  <div>
                    <div v-if="item.bold" id=selectedchild @click="item.bold=!item.bold"><b>Gras</b></div>
                    <div v-if="!item.bold" class="unselectedchild" @click="item.bold=!item.bold"><b>Gras</b></div>
                    <div v-if="item.italic" id=selectedchild @click="item.italic=!item.italic"><i>Italique</i></div>
                    <div v-if="!item.italic" class="unselectedchild" @click="item.italic=!item.italic"><i>Italique</i></div>
                    <div v-if="item.underline" id=selectedchild @click="item.underline=!item.underline"><div class="underlined">Souligné</div></div>
                    <div v-if="!item.underline" class="unselectedchild" @click="item.underline=!item.underline"><div class="underlined">Souligné</div></div>
                    <div v-if="item.strikethrough" id=selectedchild @click="item.strikethrough=!item.strikethrough"><div class="strikethrough">Barré</div></div>
                    <div v-if="!item.strikethrough" class="unselectedchild" @click="item.strikethrough=!item.strikethrough"><div class="strikethrough">Barré</div></div>
                    <div v-if="item.textsize!='large'" class="unselectedchild" @click="item.textsize='large'"><div class="big">Texte de grande taille</div></div>
                    <div v-if="item.textsize=='large'" id=selectedchild @click="item.textsize='large'"><div class="big">Texte de grande taille</div></div>
                    <div v-if="item.textsize!='medium'" class="unselectedchild" @click="item.textsize='medium'">Texte de taille moyenne</div>
                    <div v-if="item.textsize=='medium'" id=selectedchild @click="item.textsize='medium'">Texte de taille moyenne</div>
                    <div v-if="item.textsize!='small'" class="unselectedchild" @click="item.textsize='small'"><div class="small">Texte de petite taille</div></div>
                    <div v-if="item.textsize=='small'" id=selectedchild @click="item.textsize='small'"><div class="small">Texte de petite taille</div></div>
                  </div>
                  <br>
                  <br>
                  <div><b>Changer la couleur de l'élément</b></div>
                  <div v-bind:style="{color:item.color}" class="colordisplay">Couleur actuelle: {{item.color}}</div>
                  <select v-model="item.color">
                    <option v-for="colorchoice in this.colorlist" v-bind:style="{color:colorchoice}">{{colorchoice}}</option>
                  </select>
                  <div v-bind:style="{backgroundColor:item.backgroundcolor}" class="backgroundcolordisplay">Couleur de fond actuelle: {{item.backgroundcolor}}</div>
                  <select v-model="item.backgroundcolor">
                    <option v-for="colorchoice in this.colorlist" v-bind:style="{color:colorchoice}">{{colorchoice}}</option>
                  </select>
                </div>
              </div>
            </div>

          </div>

          <button class="mt-2 float-end btn btn-sm btn-danger" @click="$emit('delete-item')" v-if="unfold">Effacer tag</button>
        </template>
      </div>
    </div>
  </div> <!-- col -->
  `

}
