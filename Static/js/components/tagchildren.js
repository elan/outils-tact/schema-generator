export default {
  props: ['child', 'parent', 'childlist'],
  data() {
    return {}
  },
  emits: ['newchild', 'removechild'],
  methods: {
    addchild(child) {
      this.$emit('newchild', child)
    },
    deletechild(child) {
      this.$emit('removechild', child)
    }
  },
  template: `
    <div class="input-group mb-1">
      <span class="input-group-text form-control" v-bind:class="{'text-muted': !this.childlist.includes(child)}">{{child}}</span>
      <button @click="addchild(child)" class="btn btn-sm btn-secondary" v-if="!this.childlist.includes(child)"> +</button>
      <button @click="deletechild(child)" class="btn btn-sm btn-secondary" v-if="this.childlist.includes(child)">-</button>
    </div>
`
}