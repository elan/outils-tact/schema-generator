import Entrees from "./components/entrees.js"
import Tagitem from "./components/tagitem.js"
import Tagattribute from "./components/tagattribute.js"
import Tagchildren from "./components/tagchildren.js"

const VueApp = {
  data() {
    return {
      tags: [],
      count: 0,
      err: false,
      saved: false,
      downloadurl: "",
      downloadurlcss: "",
      importedfilename: "fichier_resultat.json",
      cssfilename: "style_resultat.css",
      browsefile: ""
    }
  },
  methods: {
    /* inutile, a enlever*/
    importJson() {
      this.tags = JSON.parse(this.$refs.jsonimport.value)
      this.importedfilename = this.$refs.filename.value
      this.count = this.$refs.number.value
    },
    addTag(message) {
      if (!this.tags.some((element) => element.tag == message)) {
        this.tags.push({
            'tag': message,
            'id': this.count,
            'help': "",
            'rootDisplayed': true,
            'attributes': [],
            'children': [],
            'bold': false,
            'italic': false,
            'underline': false,
            'strikethrough': false,
            'textsize': 'medium',
            'color': "black",
            'backgroundcolor': "white"
          }),
          this.count++
        this.message = ""
      } else {
        this.err = true
      }
    },
    removeItem(index) {
      var deleted_tag = this.tags[index].tag
      if (this.tags.length == 1) {
        this.tags = []
        this.count--
      } else if (index + 1 == this.tags.length) {
        this.tags.pop()
        this.count--
        this.tags.forEach(item => {
          if (item.id > index) {
            item.id--
          }
          item.children.splice(item.children.indexOf(deleted_tag), 1)
        })
      } else if (index < this.tags.length) {
        this.tags.splice(index, 1)
        this.count--
        this.tags.forEach(item => {
          if (item.id > index) {
            item.id--
          }
          item.children.splice(item.children.indexOf(deleted_tag), 1)
        })
      }
    },
    removeError() {
      this.err = false
    },
    reset() {
      this.tags = []
      this.count = 0
    },
    save() {
      let jsonresult = {
        elements: this.tags
      }
      var data = new Blob([JSON.stringify(jsonresult, null, 4)], {
        type: 'application/json'
      })
      var prefix = "#preview "
      var csstext = ""
      jsonresult.elements.forEach((item) => {
        var tag = item.tag.replace(":", '\\:')
        csstext += prefix + tag + " {\n\tcolor: " + item.color + ";\n"
        csstext += "\tbackground-color: " + item.backgroundcolor + ";\n"
        if (item.textsize == "large") {
          csstext += "\tfont-size: x-large;\n"
        }
        if (item.textsize == "small") {
          csstext += "\tfont-size: small;\n"
        }
        if (item.bold) {
          csstext += "\tfont-weight: bold;\n"
        }
        if (item.italic) {
          csstext += "\tfont-style: italic;\n"
        }
        if (item.underline && item.strikethrough) {
          csstext += "\ttext-decoration: underline line-through;\n"
        } else if (item.underline) {
          csstext += "\ttext-decoration: underline;\n"
        } else if (item.strikethrough) {
          csstext += "\ttext-decoration: line-through;\n"
        }
        csstext += "}\n\n"
      })
      var cssdata = new Blob([csstext], {
        type: 'text/plain'
      })
      var url = window.URL.createObjectURL(data)
      var urlcss = window.URL.createObjectURL(cssdata)
      this.downloadurl = url
      this.downloadurlcss = urlcss
      this.saved = true
      alert("Changements sauvegardés, le fichier peut maintenant être téléchargé")
    },
    erasesave() {
      this.downloadurl = ""
      this.saved = false
    },
    fileload(event) {
      const file = event.target.files[0]
      this.importedfilename = file.name
      var that = this
      fileread(file, function(answer) {
        that.tags = JSON.parse(answer.target.result).elements
        that.count = 0
        that.tags.forEach(item => {
          if (!item.hasOwnProperty("id")) {
            item.id = that.count
          }
          if (!item.hasOwnProperty("help")) {
            item.help = ""
          }
          if (!item.hasOwnProperty("rootDisplayed")) {
            item.rootDisplayed = true
          }
          if (!item.hasOwnProperty("children")) {
            item.children = []
          }
          if (!item.hasOwnProperty("bold")) {
            item.bold = false
          }
          if (!item.hasOwnProperty("italic")) {
            item.italic = false
          }
          if (!item.hasOwnProperty("underline")) {
            item.underline = false
          }
          if (!item.hasOwnProperty("strikethrough")) {
            item.strikethrough = false
          }
          if (!item.hasOwnProperty("textsize")) {
            item.textsize = "medium"
          }
          if (!item.hasOwnProperty("color") || item.color == "") {
            item.color = "black"
          }
          if (!item.hasOwnProperty("backgroundcolor")) {
            item.backgroundcolor = "white"
          }
          that.count++
        })
      })

      function fileread(file, callbackfunction) {
        var reader = new FileReader()
        reader.onload = callbackfunction
        reader.readAsText(file)
      }

    }
  }
}

const appli = Vue.createApp(VueApp)

appli.component("entrees", Entrees)
appli.component("tag-item", Tagitem)
appli.component("tag-attribute", Tagattribute)
appli.component("tag-children", Tagchildren)

appli.mount('#vue')