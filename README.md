# TACT Schema Generator

Permet de générer un schema de transcription au format JSON exploitable dans TACT, ainsi qu'une feuille de style permettant de personnaliser la prévisualisation.

## Dépendances
- [Vue.js](https://vuejs.org/) v3
- [Bootstrap](https://getbootstrap.com/)

## Auteurs
* Julien fagot
* Arnaud Bey

## Licence
GNU AFFERO GENERAL PUBLIC LICENSE Version 3

## Utilisation

### Import de fichier

![Import de fichier](img/img2.png "Import fichier")
*Import de fichier*

Le bouton "import de fichier Json" permet de commencer à travailler à partir d'un ficher déjà existant. Il fait apparaître une option permettant de parcourir les fichiers depuis votre ordinateur et d'en choisir un à importer. Une fois le fichier sélectionné, toutes les étiquettes qu'il contient apparaîtront sur le site et pourront être modifiées.

### Créer une nouvelle étiquette

![Création d'étiquette](img/img3.png "Création d'étiquette")
*Création d'étiquette*

Il est aussi possible de créer de nouvelles étiquettes. Pour ce faire, il faut entrer le nom de la nouvelle étiquette dans le champ de texte "ajout d'un nouvel élément". Un bouton avec une flèche verte permettant de valider la création apparaîtra si le nom est valide, c-est-à-dire si il ne s'agit pas d'un doublon et qu'il ne contient aucun caractère spécial non autorisé en Json.

### Modifier ou effacer une étiquette existante

![Modification](img/img4.png "Modification")
*Modification d'étiquette*

Toutes les étiquettes crées ou importées apparaissent sous la forme de cadres indiquant le nom de l'étiquette. Cliquer sur la flèche à côté du nom d'une étiquette permet d'ouvrir le menu d'édition de cette étiquette.

![Menu d'édition](img/img5.png "Menu d'édition")
*Menu d'édition*

Le menu d'édition permet d'éditer cinq différents paramètres de l'étiquette.

![Message d'aide](img/img6.png "Message d'aide")
*Message d'aide*

Cette première option permet d'ajouter un message d'aide à l'étiquette. Ce message pourra ensuite être utilisé pour fournir des informations sur l'étiquette à tout utilisateur.

![Attributs](img/img7.png "Attributs")
*Attributs*

Cette option permet d'ajouter des attributs à l'étiquette, ainsi que d'éditer tout attribut déjà présent.

![Edition d'attribut](img/img12.png "Edition d'attribut")
*Edition d'attribut*

Il est possible de modifier le nom de l'attribut, de choisir si il est obligatoire ou facultatif, de choisir si il accepte toute valeur ou si il n'accepte qu'un nombre limité de valeurs parmi une liste prédéterminée. Il est aussi possible d'effacer l'attribut.

![Sous-éléments](img/img8.png "Sous-éléments")
*Sous-éléments*

Cette option permet de définir les sous-éléments que doit accepter l'étiquette. En cliquant sur le bouton "+", on autorise l'élément indique à être un sous-élément de l'élément que l'on est en trait d'éditer. Deux boutons permettent de rapidement ajouter ou retirer tous les éléments possibles de la liste des sous-éléments.

![Autres options](img/img9.png "Autres options")
*Autres options*

Cette option permet de déterminer des options supplémentaires. La seule option supplémentaire disponible pour le moment est de déterminer si l'étiquette peut être utilisée à la racine, c'est à dire en dehors de tout autre élément.

![Options de style](img/img10.png "Options de style")
**

Cette option permet d'ajouter des effets visuels à tout texte contenu dans cet élément. Il est possible de rendre le texte gras, italique, de le souligner, de le barrer, d'augmenter ou réduire sa taille, de changer la couleur de texte ainsi que la couleur de fond. Ces changements de style ne sont pas ajoutés au fichier Json, mais ils sont utilisés pour créer un fichier de type CSS contenant toutes les informations de style.

Enfin, il est aussi possible de supprimer l'étiquette à l'aide du bouton "effacer tag".

### Export de fichier

![Export de fichier](img/img11.png "Export de fichier")
*Export de fichier*

Une fois les modifications terminées, il est possible de sauvegarder le fichier modifié. Pour ce faire, il faut cliquer sur le bouton "sauvegarder Json" pour enregistrer l'état actuel du fichier (Attention, si le fichier est modifié après avoir été enregistré, il faut l'enregistrer une nouvelle fois pour ajouter toutes les modifications). Une fois le fichier sauvegardé, trois nouvelles options apparaissent: la première ("Télécharger fichier Json") permet de télécharger le fichier Json sauvegardé, la deuxième ("Télécharger fichier CSS") permet de télécharger le fichier contenant les adaptations de style, et la troisième ("Effacer fichier Json sauvegardé") permet d'effacer le fichier Json sauvegardé.

L'option "Remise à zéro" permet de tout effacer et de revenir à la situation par défaut. Elle efface toutes les étiquettes ainsi que tout éventuel fichier sauvegardé.
